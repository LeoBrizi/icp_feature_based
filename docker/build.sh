#!/bin/sh

IMAGE_NAME=workspace #if you change it, change also the one in run.sh

cp $XAUTHORITY Xauthority

docker build -t ${IMAGE_NAME} --build-arg uid=$(id -g) --build-arg gid=$(id -g) .

rm Xauthority
