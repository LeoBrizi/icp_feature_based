#!/bin/sh

WORKSPACE_DIR=$(pwd | sed 's,/*[^/]\+/*$,,')

docker run -ti --rm --user=$(id -u $USER):$(id -g $USER) --env="DISPLAY" -v /tmp/.X11-unix:/tmp/.X11-unix:rw --privileged --gpus all --net=host --pid=host --ipc=host -v ${WORKSPACE_DIR}/dataset:/home/ubuntu/dataset -v ${WORKSPACE_DIR}/icp_feature_based_ros:/home/ubuntu/catkin_ws/src/icp_feature_based_ros/ workspace /bin/bash -c "export TORCH_DIR=/home/ubuntu/libraries/libtorch/share/cmake/Torch && cd catkin_ws/ && source /opt/ros/noetic/setup.bash && catkin_make && cd || cd && /bin/bash"
