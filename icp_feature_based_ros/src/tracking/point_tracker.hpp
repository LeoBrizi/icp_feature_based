#pragma once

#include <deque>
#include <opencv2/core.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/opencv.hpp>
#include <vector>

enum class Matcher { BFMatcher, FLANNMatcher };

class PointTracker {
 public:
  explicit PointTracker(Matcher matcher, float nn_thresh, uint max_lenght);
  explicit PointTracker(Matcher matcher, float nn_thresh);
  explicit PointTracker(Matcher matcher);
  void Update(const std::vector<cv::KeyPoint>& keypoints,
              const cv::Mat& descriptors);
  std::vector<cv::DMatch> Match(const cv::Mat& descriptors_1,
                                const cv::Mat& descriptors_2) const;
  template <int I>
  void DrawTracks(const cv::Mat& image) const;

 private:
  void InitializeTracks(const std::vector<cv::KeyPoint>& keypoints,
                        const cv::Mat& descriptors);
  void ResetTracks();
  float nn_thresh_ = 0.7f;
  uint max_lenght_ = 4;
  cv::Ptr<cv::DescriptorMatcher> matcher_;
  std::vector<std::deque<cv::Point>> tracks_;
  cv::Mat last_desc_;
};
