#include "point_tracker.hpp"

#include <omp.h>

#include <Eigen/Core>
#include <utilities/cv_types.hpp>

using cv::DMatch;
using cv::KeyPoint;
using cv::Mat;
using cv::Point;
using Eigen::Array;
using Eigen::Dynamic;
using std::deque;
using std::vector;
using ArrayXi = Array<int, Dynamic, 1>;

PointTracker::PointTracker(Matcher matcher, float nn_thresh, uint max_lenght)
    : nn_thresh_(nn_thresh), max_lenght_(max_lenght) {
  switch (matcher) {
    case Matcher::BFMatcher:
      matcher_ = cv::DescriptorMatcher::create("BruteForce");
      break;
    case Matcher::FLANNMatcher:
      matcher_ = cv::DescriptorMatcher::create("FlannBased");
      break;
  }
};

PointTracker::PointTracker(Matcher matcher, float nn_thresh)
    : nn_thresh_(nn_thresh) {
  switch (matcher) {
    case Matcher::BFMatcher:
      matcher_ = cv::DescriptorMatcher::create("BruteForce");
      break;
    case Matcher::FLANNMatcher:
      matcher_ = cv::DescriptorMatcher::create("FlannBased");
      break;
  }
};

PointTracker::PointTracker(Matcher matcher) {
  switch (matcher) {
    case Matcher::BFMatcher:
      matcher_ = cv::DescriptorMatcher::create("BruteForce");
      break;
    case Matcher::FLANNMatcher:
      matcher_ = cv::DescriptorMatcher::create("FlannBased");
      break;
  }
};

void PointTracker::InitializeTracks(const std::vector<cv::KeyPoint>& keypoints,
                                    const cv::Mat& descriptors) {
  assert(static_cast<int>(keypoints.size()) == descriptors.rows);

  for (const auto& keypoint : keypoints) {
    deque<Point> new_track;
    new_track.push_back(keypoint.pt);

    tracks_.push_back(new_track);
  }

  last_desc_ = descriptors.clone();
  return;
};

void PointTracker::ResetTracks() {
  tracks_.clear();
  last_desc_.release();
  return;
};

std::vector<DMatch> PointTracker::Match(const Mat& descriptors_1,
                                        const Mat& descriptors_2) const {
  assert(descriptors_1.cols == descriptors_2.cols);

  vector<vector<DMatch>> knn_matches;

  matcher_->knnMatch(descriptors_1, descriptors_2, knn_matches, 2);

  vector<DMatch> good_matches;

  for (size_t i = 0; i < knn_matches.size(); ++i) {
    if (knn_matches[i][0].distance < nn_thresh_ * knn_matches[i][1].distance) {
      good_matches.push_back(knn_matches[i][0]);
    }
  }

  return good_matches;
};

void PointTracker::Update(const std::vector<KeyPoint>& keypoints,
                          const Mat& descriptors) {
  assert(static_cast<int>(keypoints.size()) == descriptors.rows);

  if (keypoints.size() == 0) {
    ResetTracks();
    return;
  }

  if (tracks_.size() == 0) {
    InitializeTracks(keypoints, descriptors);
    return;
  }

  ArrayXi matched_mask = ArrayXi::Zero(tracks_.size(), 1);
  ArrayXi new_points_mask = ArrayXi::Ones(keypoints.size(), 1);

  vector<DMatch> matches = Match(last_desc_, descriptors);

  vector<Mat> desc_vector(tracks_.size());

  for (const auto& match : matches) {
    tracks_[match.queryIdx].push_back(keypoints[match.trainIdx].pt);

    if (tracks_[match.queryIdx].size() > max_lenght_)
      tracks_[match.queryIdx].pop_front();

    matched_mask[match.queryIdx] = 1;
    new_points_mask[match.trainIdx] = 0;

    desc_vector[match.queryIdx] = descriptors.row(match.trainIdx).clone();
  }

  for (int i = tracks_.size() - 1; i >= 0; --i) {
    if (matched_mask[i] != 1) {
      tracks_.erase(tracks_.begin() + i);
      desc_vector.erase(desc_vector.begin() + i);
    }
  }

  for (size_t i = 0; i < keypoints.size(); ++i) {
    if (new_points_mask[i] != 0) {
      deque<Point> new_track;
      new_track.push_back(keypoints[i].pt);

      tracks_.push_back(new_track);
      desc_vector.push_back(descriptors.row(i).clone());
    }
  }

  last_desc_.release();
  last_desc_ = Mat(cv::Size(256, desc_vector.size()), CV_32F);

  for (size_t i = 0; i < desc_vector.size(); ++i) {
    desc_vector[i].copyTo(last_desc_(cv::Rect(0, i, 256, 1)));
  }

  return;
};

template <int I>
void PointTracker::DrawTracks(const Mat& image) const {
#pragma omp parallel for
  for (const auto& track : tracks_) {
    for (size_t i = 0; i < track.size() - 1; ++i) {
      cv::circle(image, track[i], 1.0,
                 cv::Scalar(0, 0, 1.f * CvType<I>::max_val), -1);
      cv::line(image, track[i], track[i + 1],
               cv::Scalar((1.f + 1.f / max_lenght_ * i) * CvType<I>::max_val, 0,
                          (1.f - 1.f / max_lenght_ * i) * CvType<I>::max_val),
               1);
    }
    if (track.size() > 1) {
      cv::circle(image, track[track.size() - 1], 3.0,
                 cv::Scalar(1.f * CvType<I>::max_val, 0, 0), -1);
    } else {
      cv::circle(image, track[track.size() - 1], 2.0,
                 cv::Scalar(0, 1.f * CvType<I>::max_val, 0), -1);
    }
  }
  return;
};

template void PointTracker::DrawTracks<CV_8U>(const Mat& image) const;

template void PointTracker::DrawTracks<CV_32F>(const Mat& image) const;

template void PointTracker::DrawTracks<CV_64F>(const Mat& image) const;