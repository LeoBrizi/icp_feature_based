#include <jsoncpp/json/json.h>
#include <nav_msgs/Odometry.h>
#include <ros/package.h>
#include <sensor_msgs/PointCloud2.h>
#include <tf/transform_broadcaster.h>

#include <chrono>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <preprocessing/lidar_projector.hpp>
#include <preprocessing/superpoint.hpp>
#include <registration/sparse_registrator.hpp>
#include <utilities/input_parser.hpp>
#include <utilities/point_cloud2_parser.hpp>

#include "ros/ros.h"

using std::cout;

#define OS1_CONF_FILE "/configurations/os1.conf"
#define RANSAC_CONF_FILE "/configurations/ransac.conf"
#define SP_CONF_FILE "/configurations/superpoint.conf"
#define TOPIC_NAME "/os1_cloud_node/points"

void PrintHelp() {
  cout << "Usage: register_pointcloud [OPTIONS] [FILE]\n";
  cout << "  It extract superpoint feature from intensity information of the "
          "lidar and performs a sparse registration\n";
  cout << "  -c    os1 config file\n";
  cout << "  -s    superpoint config file\n";
  cout << "  -r    ransac config file\n";
  cout << "  -t    ros topic name[default: ";
  cout << TOPIC_NAME;
  cout << "]\n";
  cout << "  -h    display this help and exit\n";
  cout << "Exit status:\n";
  cout << "  0 if ok\n";
  cout << "  1 there was an error\n";
}

int main(int argc, char** argv) {
  ros::init(argc, argv, "register_pointcloud");
  ros::NodeHandle nh;

  InputParser input(argc, argv);

  if (input.CmdOptionExists("-h")) {
    PrintHelp();
    exit(0);
  }

  std::string topic = TOPIC_NAME;
  if (input.CmdOptionExists("-t")) {
    topic = input.GetCmdOption("-t");
  } else {
    cout << "Using default ros topic for reading point clouds\n";
  }

  auto package_path = ros::package::getPath("icp_feature_based");

  std::string os1_config_file_name = OS1_CONF_FILE;
  if (input.CmdOptionExists("-c") &&
      std::filesystem::exists(input.GetCmdOption("-c"))) {
    os1_config_file_name = input.GetCmdOption("-c");
  } else {
    cout << "Loading OS1 default configurations\n";
    os1_config_file_name = package_path + os1_config_file_name;
  }

  cout << "LOADING OS1 configurations from :" << os1_config_file_name << "\n";

  std::ifstream os1_config_file(os1_config_file_name);

  Json::Reader reader;
  Json::Value os1_config;
  reader.parse(os1_config_file, os1_config);
  uint height = os1_config["Os1Lidar"]["vertical_resolution"].asUInt();
  uint width = os1_config["Os1Lidar"]["horizon_resolution"].asUInt();
  float fov_up = os1_config["Os1Lidar"]["fov"]["vertical_up"].asFloat();
  float fov_down = os1_config["Os1Lidar"]["fov"]["vertical_down"].asFloat();
  float max_depth = os1_config["Os1Lidar"]["max_depth"].asFloat();
  float max_intensity = os1_config["Os1Lidar"]["max_intensity"].asFloat();

  std::string sp_config_file_name = SP_CONF_FILE;
  if (input.CmdOptionExists("-s") &&
      std::filesystem::exists(input.GetCmdOption("-s"))) {
    sp_config_file_name = input.GetCmdOption("-s");
  } else {
    cout << "Loading superpoint default configurations\n";
    sp_config_file_name = package_path + sp_config_file_name;
  }

  cout << "LOADING superpoint configurations from :" << sp_config_file_name
       << "\n";

  std::ifstream sp_config_file(sp_config_file_name);
  Json::Value sp_config;
  reader.parse(sp_config_file, sp_config);
  float sp_threshold = sp_config["SuperPoint"]["threshold"].asFloat();
  int nms_distance = sp_config["SuperPoint"]["nms_dist"].asInt();

  std::string ransac_config_file_name = RANSAC_CONF_FILE;
  if (input.CmdOptionExists("-r") &&
      std::filesystem::exists(input.GetCmdOption("-r"))) {
    ransac_config_file_name = input.GetCmdOption("-r");
  } else {
    cout << "Loading ransac default configurations\n";
    ransac_config_file_name = package_path + ransac_config_file_name;
  }

  cout << "LOADING ransac configurations from :" << ransac_config_file_name
       << "\n";

  std::ifstream ransac_config_file(ransac_config_file_name);
  Json::Value ransac_config;
  reader.parse(ransac_config_file, ransac_config);
  int ransac_iterations = ransac_config["Ransac"]["iterations"].asInt();
  float inliers_threshold =
      ransac_config["Ransac"]["inliers_threshold"].asFloat();

  PointCloud2Parser cloud2_parser;

  std::unique_ptr<LidarProjector> projector = std::make_unique<LidarProjector>(
      height, width, fov_up, fov_down, max_depth, max_intensity);

  std::unique_ptr<SuperPointDetector> sp_detector =
      std::make_unique<SuperPointDetector>(-1, sp_threshold, nms_distance,
                                           true);

  SparseRegistrator registrator(*(sp_detector.get()), *(projector.get()),
                                ransac_iterations, inliers_threshold);

  PoseEstimator::Pose pose(Eigen::Matrix4f::Identity(4, 4));

  auto cloud_handler =
      [&](const sensor_msgs::PointCloud2::ConstPtr& cloud_msg) {
        auto start = std::chrono::steady_clock::now();

        auto cloud = cloud2_parser.PointCloud2ToPointCloud(cloud_msg);

        auto [found, transformation] = registrator.Register(*cloud);

        auto end = std::chrono::steady_clock::now();
        auto diff = end - start;
        cout << "cloud processed in: ";
        cout << std::chrono::duration<double, std::milli>(diff).count()
             << " ms\n";
        if (found) {
          cout << "relative transformation: " << std::endl
               << transformation->H() << std::endl;
          pose = pose * *(transformation);
          cout << "absolute pose: " << std::endl << pose.H() << std::endl;
        }
      };

  auto pc_sub = nh.subscribe<sensor_msgs::PointCloud2>(topic, 1, cloud_handler);

  ros::spin();
  exit(0);
}
