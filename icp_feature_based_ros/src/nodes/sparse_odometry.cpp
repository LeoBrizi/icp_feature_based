#include <jsoncpp/json/json.h>
#include <nav_msgs/Odometry.h>
#include <ros/package.h>
#include <sensor_msgs/PointCloud2.h>
#include <tf/transform_broadcaster.h>

#include <chrono>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <preprocessing/lidar_projector.hpp>
#include <preprocessing/superpoint.hpp>
#include <registration/sparse_registrator.hpp>
#include <utilities/input_parser.hpp>
#include <utilities/point_cloud2_parser.hpp>

#include "ros/ros.h"

using std::cout;

#define OS1_TOPIC_NAME "/os1_cloud_node/points"
#define ODOM_TOPIC_NAME "os1_odom"
#define DATASET_CONFIG "/configurations/ipb_car.conf"
#define FORWARD_CLOUD_TOPIC "sparse_odometry/points"
#define SPARSE_CLOUD_TOPIC "sparse_odometry/features_cloud"
#define SPARSE_CLOUD_INLIERS_TOPIC "sparse_odometry/features_inliers_cloud"

void PrintHelp() {
  cout << "Usage: sparse_odometry [OPTIONS] [FILE]\n";
  cout << "  It provides odometry information over the rostopic \"odom\" \n";
  cout << "  -c    dataset config file\n";
  cout << "  -t    ros topic name[default: ";
  cout << OS1_TOPIC_NAME;
  cout << "]\n";
  cout << "  -h    display this help and exit\n";
  cout << "Exit status:\n";
  cout << "  0 if ok\n";
  cout << "  1 there was an error\n";
}

int main(int argc, char** argv) {
  ros::init(argc, argv, "sparse_odometry");
  ros::NodeHandle nh;

  ros::Publisher odom_pub =
      nh.advertise<nav_msgs::Odometry>(ODOM_TOPIC_NAME, 50);

  ros::Publisher cloud_pub =
      nh.advertise<sensor_msgs::PointCloud2>(FORWARD_CLOUD_TOPIC, 50);

  ros::Publisher sparse_cloud_pub =
      nh.advertise<sensor_msgs::PointCloud2>(SPARSE_CLOUD_TOPIC, 50);

  ros::Publisher sparse_inliers_cloud_pub =
      nh.advertise<sensor_msgs::PointCloud2>(SPARSE_CLOUD_INLIERS_TOPIC, 50);

  tf::TransformBroadcaster broadcaster;

  InputParser input(argc, argv);

  if (input.CmdOptionExists("-h")) {
    PrintHelp();
    exit(0);
  }

  std::string topic = OS1_TOPIC_NAME;
  if (input.CmdOptionExists("-t")) {
    topic = input.GetCmdOption("-t");
  } else {
    cout << "Using default ros topic for reading point clouds\n";
  }

  auto package_path = ros::package::getPath("icp_feature_based");

  std::string dataset_config_file_name = DATASET_CONFIG;
  if (input.CmdOptionExists("-c") &&
      std::filesystem::exists(input.GetCmdOption("-c"))) {
    dataset_config_file_name = input.GetCmdOption("-c");
  } else {
    cout << "Loading dataset default configurations\n";
    dataset_config_file_name = package_path + dataset_config_file_name;
  }

  cout << "LOADING DATASET configurations from :" << dataset_config_file_name
       << "\n";

  std::ifstream dataset_config_file(dataset_config_file_name);

  Json::Reader reader;
  Json::Value config;
  reader.parse(dataset_config_file, config);
  uint height = config["Os1Lidar"]["vertical_resolution"].asUInt();
  uint width = config["Os1Lidar"]["horizon_resolution"].asUInt();
  float fov_up = config["Os1Lidar"]["fov"]["vertical_up"].asFloat();
  float fov_down = config["Os1Lidar"]["fov"]["vertical_down"].asFloat();
  float max_depth = config["Os1Lidar"]["max_depth"].asFloat();
  float max_intensity = config["Os1Lidar"]["max_intensity"].asFloat();

  float sp_threshold = config["SuperPoint"]["threshold"].asFloat();
  int nms_distance = config["SuperPoint"]["nms_dist"].asInt();

  int ransac_iterations = config["Ransac"]["iterations"].asInt();
  float inliers_threshold = config["Ransac"]["inliers_threshold"].asFloat();

  PointCloud2Parser cloud2_parser;

  std::unique_ptr<LidarProjector> projector = std::make_unique<LidarProjector>(
      height, width, fov_up, fov_down, max_depth, max_intensity);

  std::unique_ptr<SuperPointDetector> sp_detector =
      std::make_unique<SuperPointDetector>(-1, sp_threshold, nms_distance,
                                           true);

  SparseRegistrator registrator(*(sp_detector.get()), *(projector.get()),
                                ransac_iterations, inliers_threshold);

  PoseEstimator::Pose pose(Eigen::Matrix4f::Identity(4, 4));

  auto cloud_handler =
      [&](const sensor_msgs::PointCloud2::ConstPtr& cloud_msg) {
        auto start = std::chrono::steady_clock::now();

        auto cloud = cloud2_parser.PointCloud2ToPointCloud(cloud_msg);

        auto [found, transformation] = registrator.Register(*cloud);

        auto end = std::chrono::steady_clock::now();
        auto diff = end - start;
        cout << "cloud processed in: ";
        cout << std::chrono::duration<double, std::milli>(diff).count()
             << " ms\n";
        if (found) {
          pose = pose * *(transformation);
          cout << pose.H() << std::endl;

          Eigen::Quaternionf quaternion(pose.R());

          // first, we'll publish the transform over tf
          geometry_msgs::TransformStamped odom_trans;
          odom_trans.header.stamp = ros::Time::now();
          odom_trans.header.frame_id = "initial_pose";
          odom_trans.child_frame_id = cloud_msg->header.frame_id;

          odom_trans.transform.translation.x = pose.t().x();
          odom_trans.transform.translation.y = pose.t().y();
          odom_trans.transform.translation.z = pose.t().z();
          odom_trans.transform.rotation.x = quaternion.x();
          odom_trans.transform.rotation.y = quaternion.y();
          odom_trans.transform.rotation.z = quaternion.z();
          odom_trans.transform.rotation.w = quaternion.w();
          // send the transform
          broadcaster.sendTransform(odom_trans);

          // next, we'll publish the odometry message over ROS
          nav_msgs::Odometry odom;
          odom.header.stamp = ros::Time::now();
          odom.header.frame_id = "initial_pose";

          // set the position
          odom.pose.pose.position.x = pose.t().x();
          odom.pose.pose.position.y = pose.t().y();
          odom.pose.pose.position.z = pose.t().z();
          odom.pose.pose.orientation.x = quaternion.x();
          odom.pose.pose.orientation.y = quaternion.y();
          odom.pose.pose.orientation.z = quaternion.z();
          odom.pose.pose.orientation.w = quaternion.w();
          odom.child_frame_id = cloud_msg->header.frame_id;

          sensor_msgs::PointCloud2 forward_cloud;
          forward_cloud.header = cloud_msg->header;
          forward_cloud.header.stamp = ros::Time::now();
          forward_cloud.header.frame_id = "os1_lidar";
          forward_cloud.height = cloud_msg->height;
          forward_cloud.width = cloud_msg->width;
          forward_cloud.fields = cloud_msg->fields;
          forward_cloud.is_bigendian = cloud_msg->is_bigendian;
          forward_cloud.point_step = cloud_msg->point_step;
          forward_cloud.row_step = cloud_msg->row_step;
          forward_cloud.data = cloud_msg->data;
          forward_cloud.is_dense = cloud_msg->is_dense;

          auto features_cloud = registrator.GetFeaturesCloud();
          features_cloud.header = forward_cloud.header;

          auto features_inliers_cloud = registrator.GetFeaturesInliersCloud();
          features_inliers_cloud.header = forward_cloud.header;

          // publish the message
          odom_pub.publish(odom);
          cloud_pub.publish(forward_cloud);
          sparse_cloud_pub.publish(features_cloud);
          sparse_inliers_cloud_pub.publish(features_inliers_cloud);
        }
      };

  auto pc_sub = nh.subscribe<sensor_msgs::PointCloud2>(topic, 1, cloud_handler);

  ros::spin();
  exit(0);
}
