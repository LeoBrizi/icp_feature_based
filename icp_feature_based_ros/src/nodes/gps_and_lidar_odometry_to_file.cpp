#include <message_filters/subscriber.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <message_filters/time_synchronizer.h>
#include <nav_msgs/Odometry.h>
#include <ros/ros.h>

#include <fstream>
#include <iostream>
#include <utilities/input_parser.hpp>

#define GPS_ODOMETRY_TOPIC "gps_odom"
#define LIDAR_ODOMETRY_TOPIC "/os1_odom"

using std::cout;

void PrintHelp() {
  cout << "Usage: gps_and_lidar_odometry_to_file [OPTIONS] [FILE]\n";
  cout << "  it dumps trajectories estimated by gps and lidar\n";
  cout << "  -f    file\n";
  cout << "  -h    display this help and exit\n";
  cout << "Exit status:\n";
  cout << "  0 if ok\n";
  cout << "  1 there was an error\n";
}

std::ofstream trajectory_file;

void callback(const nav_msgs::Odometry& gps_odom,
              const nav_msgs::Odometry& lidar_odom) {
  trajectory_file << gps_odom.pose.pose.position.x << ","
                  << gps_odom.pose.pose.position.y << ","
                  << gps_odom.pose.pose.position.z << ",";
  trajectory_file << lidar_odom.pose.pose.position.x << ","
                  << lidar_odom.pose.pose.position.y << ","
                  << lidar_odom.pose.pose.position.z;
  trajectory_file << std::endl;
};

typedef message_filters::sync_policies::ApproximateTime<nav_msgs::Odometry,
                                                        nav_msgs::Odometry>
    ApproximateTimePolicy;

int main(int argc, char** argv) {
  ros::init(argc, argv, "gps_and_lidar_odometry_to_file");

  InputParser input(argc, argv);

  if (input.CmdOptionExists("-h")) {
    PrintHelp();
    exit(0);
  }

  std::string file_name;
  if (input.CmdOptionExists("-f")) {
    file_name = input.GetCmdOption("-f");
  } else {
    exit(1);
  }

  ros::NodeHandle nh;

  trajectory_file.open(file_name);

  message_filters::Subscriber<nav_msgs::Odometry> gps_sub(
      nh, GPS_ODOMETRY_TOPIC, 1);
  message_filters::Subscriber<nav_msgs::Odometry> lidar_sub(
      nh, LIDAR_ODOMETRY_TOPIC, 1);

  message_filters::Synchronizer<ApproximateTimePolicy> sync(
      ApproximateTimePolicy(10), gps_sub, lidar_sub);
  sync.registerCallback(callback);

  // message_filters::TimeSynchronizer<nav_msgs::Odometry, nav_msgs::Odometry>
  //    sync(gps_sub, lidar_sub, 10);
  // sync.registerCallback(callback);

  while (nh.ok()) {
    ros::spinOnce();
  }

  trajectory_file.close();

  exit(0);
}