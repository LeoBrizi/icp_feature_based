#include <jsoncpp/json/json.h>
#include <ros/package.h>
#include <sensor_msgs/PointCloud2.h>

#include <chrono>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <preprocessing/lidar_projector.hpp>
#include <utilities/input_parser.hpp>
#include <utilities/point_cloud2_parser.hpp>

#include "ros/ros.h"

using std::cout;

#define OS1_CONF_FILE "/configurations/os1.conf"
#define TOPIC_NAME "/os1_cloud_node/points"

void PrintHelp() {
  cout << "Usage: visualize_dataset [OPTIONS] [FILE]\n";
  cout << "  It visualizes point clouds once they are projected on images\n";
  cout << "  -c    os1 config file\n";
  cout << "  -t    ros topic name for point clouds[default: ";
  cout << TOPIC_NAME;
  cout << "]\n";
  cout << "  -h    display this help and exit\n";
  cout << "Exit status:\n";
  cout << "  0 if ok\n";
  cout << "  1 there was an error\n";
}

int main(int argc, char** argv) {
  ros::init(argc, argv, "visualize_dataset");
  ros::NodeHandle nh;

  InputParser input(argc, argv);

  if (input.CmdOptionExists("-h")) {
    PrintHelp();
    exit(0);
  }

  std::string topic = TOPIC_NAME;
  if (input.CmdOptionExists("-t")) {
    topic = input.GetCmdOption("-t");
  } else {
    cout << "Using default ros topic for reading point clouds\n";
  }

  auto package_path = ros::package::getPath("icp_feature_based");

  std::string os1_config_file_name = OS1_CONF_FILE;
  if (input.CmdOptionExists("-c") &&
      std::filesystem::exists(input.GetCmdOption("-c"))) {
    os1_config_file_name = input.GetCmdOption("-c");
  } else {
    cout << "Loading OS1 default configurations\n";
    os1_config_file_name = package_path + os1_config_file_name;
  }

  cout << "LOADING OS1 configurations from :" << os1_config_file_name << "\n";

  std::ifstream os1_config_file(os1_config_file_name);

  Json::Reader reader;
  Json::Value os1_config;
  reader.parse(os1_config_file, os1_config);
  uint height = os1_config["Os1Lidar"]["vertical_resolution"].asUInt();
  uint width = os1_config["Os1Lidar"]["horizon_resolution"].asUInt();
  float fov_up = os1_config["Os1Lidar"]["fov"]["vertical_up"].asFloat();
  float fov_down = os1_config["Os1Lidar"]["fov"]["vertical_down"].asFloat();
  float max_depth = os1_config["Os1Lidar"]["max_depth"].asFloat();
  float max_intensity = os1_config["Os1Lidar"]["max_intensity"].asFloat();

  PointCloud2Parser cloud2_parser;
  LidarProjector projector(height, width, fov_up, fov_down);

  auto cloud_handler =
      [&](const sensor_msgs::PointCloud2::ConstPtr& cloud_msg) {
        auto start = std::chrono::steady_clock::now();

        auto cloud = cloud2_parser.PointCloud2ToPointCloud(cloud_msg);

        auto image = projector.Project2Image(*cloud);

        auto cv_image =
            image->Convert2CvImage<CV_32F>(max_depth, max_intensity);

        auto end = std::chrono::steady_clock::now();
        auto diff = end - start;
        cout << "cloud processed in: ";
        cout << std::chrono::duration<double, std::milli>(diff).count()
             << " ms\n";

        cv::imshow("Depth", cv_image->depth_);
        cv::waitKey(1);
        cv::imshow("Intensity", cv_image->intensity_);
        cv::waitKey(1);
      };

  auto pc_sub = nh.subscribe<sensor_msgs::PointCloud2>(topic, 1, cloud_handler);

  ros::spin();
  exit(0);
}
