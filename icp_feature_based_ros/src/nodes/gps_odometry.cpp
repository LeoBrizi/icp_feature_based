#include <nav_msgs/Odometry.h>
#include <ros/ros.h>
#include <sensor_msgs/NavSatFix.h>
#include <tf/transform_broadcaster.h>

#define GPS_ODOMETRY_TOPIC "gps_odom"
#define GPS_MSG_TOPIC "/gps/emlid/fix"

double EarthRadiusInMeters(double latitude_radians) {
  // latitudeRadians is geodetic, i.e. that reported by GPS.
  // http://en.wikipedia.org/wiki/Earth_radius
  double a = 6378137.0;  // equatorial radius in meters
  double b = 6356752.3;  // polar radius in meters
  double cos = std::cos(latitude_radians);
  double sin = std::sin(latitude_radians);
  double t1 = a * a * cos;
  double t2 = b * b * sin;
  double t3 = a * cos;
  double t4 = b * sin;
  return std::sqrt((t1 * t1 + t2 * t2) / (t3 * t3 + t4 * t4));
}

double GeocentricLatitude(double lat) {
  // Convert geodetic latitude 'lat' to a geocentric latitude 'clat'.
  // Geodetic latitude is the latitude as given by GPS.
  // Geocentric latitude is the angle measured from center of Earth between a
  // point and the equator.
  // https://en.wikipedia.org/wiki/Latitude#Geocentric_latitude
  double e2 = 0.00669437999014;
  double clat = std::atan((1.0 - e2) * std::tan(lat));
  return clat;
}

std::tuple<float, float, float> LocationToPoint(double latitude,
                                                double longitude) {
  // Convert (lat, lon, elv) to (x, y, z).
  double lat = latitude * M_PI / 180.f;
  double lon = longitude * M_PI / 180.f;
  double radius = EarthRadiusInMeters(lat);
  double clat = GeocentricLatitude(lat);

  double cos_lon = std::cos(lon);
  double sin_lon = std::sin(lon);
  double cos_lat = std::cos(clat);
  double sin_lat = std::sin(clat);
  double x = radius * cos_lon * cos_lat;
  double y = radius * sin_lon * cos_lat;
  double z = radius * sin_lat;

  return std::make_tuple(x, y, z);
}

int main(int argc, char** argv) {
  ros::init(argc, argv, "gps_odometry");

  ros::NodeHandle nh;
  ros::Publisher odom_pub =
      nh.advertise<nav_msgs::Odometry>(GPS_ODOMETRY_TOPIC, 50);
  tf::TransformBroadcaster broadcaster;

  double initial_x = 0.0;
  double initial_y = 0.0;
  double initial_z = 0.0;

  bool first_pose = true;

  auto gps_message_handler =
      [&](const sensor_msgs::NavSatFix::ConstPtr& gps_msg) {
        double latitude = gps_msg->latitude;
        double longitude = gps_msg->longitude;

        auto [x, y, z] = LocationToPoint(latitude, longitude);

        if (first_pose) {
          initial_x = x;
          initial_y = y;
          initial_z = z;
          first_pose = false;
        }

        geometry_msgs::TransformStamped odom_trans;
        odom_trans.header.stamp = ros::Time::now();
        odom_trans.header.frame_id = "initial_pose";
        odom_trans.child_frame_id = "gps_odom";

        odom_trans.transform.translation.x = x - initial_x;
        odom_trans.transform.translation.y = y - initial_y;
        odom_trans.transform.translation.z = z - initial_z;
        odom_trans.transform.rotation.x = 0;
        odom_trans.transform.rotation.y = 0;
        odom_trans.transform.rotation.z = 0;
        odom_trans.transform.rotation.w = 1;
        // send the transform
        broadcaster.sendTransform(odom_trans);

        // next, we'll publish the odometry message over ROS
        nav_msgs::Odometry odom;
        odom.header.stamp = ros::Time::now();
        odom.header.frame_id = "initial_pose";

        // set the position
        odom.pose.pose.position.x = x - initial_x;
        odom.pose.pose.position.y = y - initial_y;
        odom.pose.pose.position.z = z - initial_z;
        odom.pose.pose.orientation.x = 0;
        odom.pose.pose.orientation.y = 0;
        odom.pose.pose.orientation.z = 0;
        odom.pose.pose.orientation.w = 1;
        odom.child_frame_id = "gps_odom";

        std::cout << "x: " << odom.pose.pose.position.x
                  << ", y: " << odom.pose.pose.position.y
                  << ", z: " << odom.pose.pose.position.z << std::endl;

        odom_pub.publish(odom);
      };

  auto gps_sub = nh.subscribe<sensor_msgs::NavSatFix>(GPS_MSG_TOPIC, 1,
                                                      gps_message_handler);
  ros::spin();
  exit(0);
}