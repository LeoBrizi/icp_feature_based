#include "point_cloud2_parser.hpp"

#include <omp.h>

using sensor_msgs::PointCloud2;
using std::make_shared;
using std::shared_ptr;

void PointCloud2Parser::InitializeParser(
    const PointCloud2::ConstPtr& input_cloud) {
  height_ = input_cloud->height;
  width_ = input_cloud->width;
  point_step_ = input_cloud->point_step;

  for (auto& field : input_cloud->fields) {
    if (field.name == "x") {
      x_offset_ = field.offset;
    } else if (field.name == "y") {
      y_offset_ = field.offset;
    } else if (field.name == "z") {
      z_offset_ = field.offset;
    } else if (field.name == "intensity") {
      i_offset_ = field.offset;
    }
  }

  return;
};

shared_ptr<PointCloudf> PointCloud2Parser::PointCloud2ToPointCloud(
    const PointCloud2::ConstPtr& input_cloud) {
  if (height_ == 0 || width_ == 0 || point_step_ == 0) {
    InitializeParser(input_cloud);
  }

  shared_ptr<PointCloudf> point_cloud =
      make_shared<PointCloudf>(height_, width_);

  const unsigned char* point_ptr = input_cloud->data.data();

  if (x_offset_ + sizeof(float) == y_offset_ &&
      y_offset_ + sizeof(float) == z_offset_ &&
      (point_step_ + x_offset_) % 16 == 0) {
#pragma omp parallel for
    for (uint i = 0; i < input_cloud->height * input_cloud->width; ++i) {
      uint point_offset = i * point_step_;
      __m128 xyz = _mm_load_ps(
          reinterpret_cast<const float*>(point_ptr + point_offset + x_offset_));

      point_cloud->x(i) = xyz[0];
      point_cloud->y(i) = xyz[1];
      point_cloud->z(i) = xyz[2];
      point_cloud->i(i) = *(
          reinterpret_cast<const float*>(point_ptr + point_offset + i_offset_));
    }
  } else {
#pragma omp parallel for
    for (uint i = 0; i < input_cloud->height * input_cloud->width - 1; ++i) {
      uint point_offset = i * point_step_;
      point_cloud->x(i) = *(
          reinterpret_cast<const float*>(point_ptr + point_offset + x_offset_));
      point_cloud->y(i) = *(
          reinterpret_cast<const float*>(point_ptr + point_offset + y_offset_));
      point_cloud->z(i) = *(
          reinterpret_cast<const float*>(point_ptr + point_offset + z_offset_));
      point_cloud->i(i) = *(
          reinterpret_cast<const float*>(point_ptr + point_offset + i_offset_));
    }
  }

  return point_cloud;
};
