#pragma once

#include <sensor_msgs/PointCloud2.h>

#include <core/point_cloud.hpp>
#include <memory>

class PointCloud2Parser {
 public:
  std::shared_ptr<PointCloudf> PointCloud2ToPointCloud(
      const sensor_msgs::PointCloud2::ConstPtr& input_cloud);

 private:
  size_t height_ = 0;
  size_t width_ = 0;
  uint16_t point_step_ = 0;
  uint16_t x_offset_ = 0;
  uint16_t y_offset_ = 0;
  uint16_t z_offset_ = 0;
  uint16_t i_offset_ = 0;
  void InitializeParser(const sensor_msgs::PointCloud2::ConstPtr& input_cloud);
};