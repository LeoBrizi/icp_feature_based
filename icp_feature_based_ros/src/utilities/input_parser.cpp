#include "input_parser.hpp"

InputParser::InputParser(int &argc, char **argv) {
  for (int i = 1; i < argc; ++i) this->tokens_.push_back(std::string(argv[i]));
}

const std::string &InputParser::GetCmdOption(const std::string &option) const {
  std::vector<std::string>::const_iterator itr;
  itr = std::find(this->tokens_.begin(), this->tokens_.end(), option);
  if (itr != this->tokens_.end() && ++itr != this->tokens_.end()) {
    return *itr;
  }
  static const std::string empty_string("");
  return empty_string;
}

int InputParser::GetInt(const std::string &option) const {
  std::vector<std::string>::const_iterator itr;
  itr = std::find(this->tokens_.begin(), this->tokens_.end(), option);
  if (itr != this->tokens_.end() && ++itr != this->tokens_.end()) {
    return std::stoi(*itr);
  }
  return 0;
}

float InputParser::GetFloat(const std::string &option) const {
  std::vector<std::string>::const_iterator itr;
  itr = std::find(this->tokens_.begin(), this->tokens_.end(), option);
  if (itr != this->tokens_.end() && ++itr != this->tokens_.end()) {
    return std::stof(*itr);
  }
  return 0.f;
}

bool InputParser::CmdOptionExists(const std::string &option) const {
  return std::find(this->tokens_.begin(), this->tokens_.end(), option) !=
         this->tokens_.end();
}
