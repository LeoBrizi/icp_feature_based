#pragma once

#include <algorithm>
#include <vector>

class InputParser {
 public:
  InputParser(int &argc, char **argv);
  const std::string &GetCmdOption(const std::string &option) const;
  int GetInt(const std::string &option) const;
  float GetFloat(const std::string &option) const;
  bool CmdOptionExists(const std::string &option) const;

 private:
  std::vector<std::string> tokens_;
};
