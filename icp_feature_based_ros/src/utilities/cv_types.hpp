#pragma once

template <int I>
struct CvType {};

template <>
struct CvType<CV_64F> {
  typedef double type_t;
  static constexpr double max_val = 1.f;
};

template <>
struct CvType<CV_32F> {
  typedef float type_t;
  static constexpr double max_val = 1.f;
};

template <>
struct CvType<CV_8U> {
  typedef unsigned char type_t;
  static const short max_val = 255;
};