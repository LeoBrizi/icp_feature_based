#pragma once
#include <memory>
#include <tuple>

class Estimator {
 public:
  struct Model {
    virtual std::shared_ptr<Model> Clone() const = 0;
  };
  struct Data {
    virtual bool IsValid(int min_obs_solution) const = 0;
  };

  virtual Model* Solve(Data& data) = 0;

  virtual std::tuple<int, std::unique_ptr<Data>> EvaluateSolution(
      Data& data, float inliers_threshold) const = 0;

  virtual std::tuple<int, std::unique_ptr<Data>> SelectBestInliers(
      Data& data, float inliers_threshold) const = 0;

  virtual std::unique_ptr<Data> SelectMinimumSolution(Data& data) const = 0;

 protected:
  std::shared_ptr<Model> solution_ = nullptr;
  int min_obs_solution_;
  friend class Ransac;
};

class Ransac {
 public:
  explicit Ransac(Estimator& estimator);
  explicit Ransac(Estimator& estimator, int iterations);
  explicit Ransac(Estimator& estimator, int iterations,
                  float inliers_threshold);
  std::tuple<bool, Estimator::Model*> FindBestSolution(Estimator::Data& data);

  std::tuple<bool, Estimator::Model*, std::unique_ptr<Estimator::Data>>
  FindBestSolutionDebug(Estimator::Data& data);

 private:
  Estimator& estimator_;
  std::shared_ptr<Estimator::Model> best_solution_;
  int iterations_ = 100;
  float inliers_threshold_ = 1e-5;
};
