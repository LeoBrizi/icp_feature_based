#include "ransac.hpp"

#include <assert.h>

#include <iostream>

Ransac::Ransac(Estimator& estimator) : estimator_{estimator} { srand(42); };

Ransac::Ransac(Estimator& estimator, int iterations)
    : estimator_{estimator}, iterations_(iterations) {
  srand(42);
};

Ransac::Ransac(Estimator& estimator, int iterations, float inliers_threshold)
    : estimator_{estimator},
      iterations_(iterations),
      inliers_threshold_(inliers_threshold) {
  srand(42);
};

std::tuple<bool, Estimator::Model*> Ransac::FindBestSolution(
    Estimator::Data& data) {
  int best_score = 0;
  std::shared_ptr<Estimator::Model> best_sol;

  for (int i = 0; i < iterations_; ++i) {
    std::unique_ptr<Estimator::Data> minimum_solution =
        estimator_.SelectMinimumSolution(data);

    Estimator::Model* model = estimator_.Solve(*minimum_solution);

    auto [score, _] = estimator_.EvaluateSolution(data, inliers_threshold_);

    if (score > best_score) {
      best_score = score;
      best_sol = model->Clone();
    }
  }

  if (!best_sol) return std::make_tuple(false, nullptr);

  estimator_.solution_ = best_sol->Clone();

  auto [score, best_inliers] =
      estimator_.SelectBestInliers(data, inliers_threshold_);

  std::cout << "solution score: " << score << std::endl;

  if (best_inliers && best_inliers->IsValid(estimator_.min_obs_solution_)) {
    Estimator::Model* model = estimator_.Solve(*best_inliers);
    best_solution_ = model->Clone();
    return std::make_tuple(true, best_solution_.get());
  }
  return std::make_tuple(false, nullptr);
};

std::tuple<bool, Estimator::Model*, std::unique_ptr<Estimator::Data>>
Ransac::FindBestSolutionDebug(Estimator::Data& data) {
  int best_score = 0;
  std::shared_ptr<Estimator::Model> best_sol;

  for (int i = 0; i < iterations_; ++i) {
    std::unique_ptr<Estimator::Data> minimum_solution =
        estimator_.SelectMinimumSolution(data);

    Estimator::Model* model = estimator_.Solve(*minimum_solution);

    auto [score, _] = estimator_.EvaluateSolution(data, inliers_threshold_);

    if (score > best_score) {
      best_score = score;
      best_sol = model->Clone();
    }
  }

  if (!best_sol) return std::make_tuple(false, nullptr, nullptr);

  estimator_.solution_ = best_sol->Clone();

  auto [score, best_inliers] =
      estimator_.SelectBestInliers(data, inliers_threshold_);

  std::cout << "solution score: " << score << std::endl;

  if (best_inliers && best_inliers->IsValid(estimator_.min_obs_solution_)) {
    Estimator::Model* model = estimator_.Solve(*best_inliers);
    best_solution_ = model->Clone();
    return std::make_tuple(true, best_solution_.get(), std::move(best_inliers));
  }
  return std::make_tuple(false, nullptr, nullptr);
};

/*
std::tuple<bool, Estimator::Model*> Ransac::FindBestSolution(
    Estimator::Data& data) {
  int best_score = 0;
  std::unique_ptr<Estimator::Data> best_inliers = nullptr;

  for (int i = 0; i < iterations_; ++i) {
    std::unique_ptr<Estimator::Data> minimum_solution =
        estimator_.SelectMinimumSolution(data);

    estimator_.Solve(*minimum_solution);

    auto [score, inliers] =
        estimator_.EvaluateSolution(data, inliers_threshold_);

    if (score > best_score) {
      best_score = score;
      best_inliers = std::move(inliers);
    }
  }

  if (best_inliers && best_inliers->IsValid(estimator_.min_obs_solution_)) {
    Estimator::Model* model = estimator_.Solve(*best_inliers);
    auto [score, inliers] =
        estimator_.EvaluateSolution(data, inliers_threshold_);
    std::cout << "solution score: " << score << std::endl;
    best_solution_ = model->Clone();
    return std::make_tuple(true, best_solution_.get());
  }
  return std::make_tuple(false, nullptr);
};
*/