#pragma once

#include <Eigen/Dense>
#include <Eigen/StdVector>

#include "ransac.hpp"

class PoseEstimator : public Estimator {
 public:
  struct Pose : Model {
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    Eigen::Matrix4f pose_;

    Pose(const Eigen::Matrix4f& pose) : pose_(pose){};
    Pose(const Eigen::Matrix3f& R, Eigen::Vector3f t) {
      pose_ = Eigen::Matrix4f::Identity(4, 4);
      pose_.block<3, 3>(0, 0) = R;
      pose_.block<3, 1>(0, 3) = t;
    };

    inline Eigen::Matrix3f R() const { return pose_.block<3, 3>(0, 0); };
    inline Eigen::Vector3f t() const { return pose_.block<3, 1>(0, 3); };
    inline Eigen::Matrix4f H() const { return pose_; };

    std::shared_ptr<Model> Clone() const override {
      return std::make_shared<Pose>(*this);
    };

    Pose operator*(const Pose& input) const {
      Eigen::Matrix3f R = this->R() * input.R();
      Eigen::Vector3f t = this->R() * input.t() + this->t();
      Pose new_pose(R, t);
      return new_pose;
    };

    Pose inverse() const {
      Eigen::Matrix3f R_inv = this->R().inverse();
      Eigen::Matrix3f R = R_inv;
      Eigen::Vector3f t = -R_inv * this->t();
      Pose new_pose(R, t);
      return new_pose;
    };
  };

  struct Points : Data {
    std::vector<Eigen::Vector3f, Eigen::aligned_allocator<Eigen::Vector3f>>
        cloud_1_;
    std::vector<Eigen::VectorXf, Eigen::aligned_allocator<Eigen::VectorXf>>
        descriptors_1_;
    std::vector<Eigen::Vector3f, Eigen::aligned_allocator<Eigen::Vector3f>>
        cloud_2_;
    std::vector<Eigen::VectorXf, Eigen::aligned_allocator<Eigen::VectorXf>>
        descriptors_2_;

    Points() = default;

    Points(
        const std::vector<Eigen::Vector3f,
                          Eigen::aligned_allocator<Eigen::Vector3f>>& cloud_1,
        const std::vector<Eigen::Vector3f,
                          Eigen::aligned_allocator<Eigen::Vector3f>>& cloud_2)
        : cloud_1_(cloud_1), cloud_2_(cloud_2){};

    Points(
        const std::vector<Eigen::Vector3f,
                          Eigen::aligned_allocator<Eigen::Vector3f>>& cloud_1,
        const std::vector<Eigen::VectorXf,
                          Eigen::aligned_allocator<Eigen::VectorXf>>&
            descriptors_1,
        const std::vector<Eigen::Vector3f,
                          Eigen::aligned_allocator<Eigen::Vector3f>>& cloud_2,
        const std::vector<Eigen::VectorXf,
                          Eigen::aligned_allocator<Eigen::VectorXf>>&
            descriptors_2)
        : cloud_1_(cloud_1),
          descriptors_1_(descriptors_1),
          cloud_2_(cloud_2),
          descriptors_2_(descriptors_2){};

    Eigen::Vector3f FindCorrispondence(const Eigen::Vector3f& point,
                                       const Eigen::VectorXf& descriptor) const;

    bool IsValid(int min_obs_solution) const override {
      if (static_cast<int>(cloud_1_.size()) > min_obs_solution &&
          static_cast<int>(cloud_2_.size()) > min_obs_solution)
        return true;
      return false;
    };
  };

  PoseEstimator() { min_obs_solution_ = 3; };

  Model* Solve(Data& Data) override;

  std::tuple<int, std::unique_ptr<Data>> EvaluateSolution(
      Data& data, float inliers_threshold) const override;

  std::tuple<int, std::unique_ptr<Data>> SelectBestInliers(
      Data& data, float inliers_threshold) const override;

  std::unique_ptr<Data> SelectMinimumSolution(Data& data) const override;

  const Pose* GetPose() const {
    assert(solution_.get() != nullptr);
    return static_cast<Pose*>(solution_.get());
  };
};