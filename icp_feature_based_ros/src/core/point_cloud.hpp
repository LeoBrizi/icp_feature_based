#pragma once

#include <Eigen/Core>

template <typename pT, typename iT>
class PointCloud {
 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

  struct Point3DI {
    pT x_;
    pT y_;
    pT z_;
    iT i_;
  };

  explicit PointCloud(size_t h_, size_t w_);
  explicit PointCloud(size_t dim);
  PointCloud();

  inline const Eigen::Array<pT, Eigen::Dynamic, 1>& x() { return this->x_; };
  inline const Eigen::Array<pT, Eigen::Dynamic, 1>& y() { return this->y_; };
  inline const Eigen::Array<pT, Eigen::Dynamic, 1>& z() { return this->z_; };
  inline const Eigen::Array<iT, Eigen::Dynamic, 1>& i() { return this->i_; };

  inline pT& x(uint index) { return this->x_(index); };
  inline pT& y(uint index) { return this->y_(index); };
  inline pT& z(uint index) { return this->z_(index); };
  inline iT& i(uint index) { return this->i_(index); };

  struct Point3DI p(uint index) const;

  inline size_t h() { return this->h_; };
  inline size_t w() { return this->w_; };

 private:
  Eigen::Array<pT, Eigen::Dynamic, 1> x_;
  Eigen::Array<pT, Eigen::Dynamic, 1> y_;
  Eigen::Array<pT, Eigen::Dynamic, 1> z_;
  Eigen::Array<iT, Eigen::Dynamic, 1> i_;
  size_t h_;
  size_t w_;
};

typedef struct PointCloud<float, float>::Point3DI Point3DIf;

typedef PointCloud<float, float> PointCloudf;