#include "pose_estimator.hpp"

#include <omp.h>
#include <stdlib.h>

#include <Eigen/SVD>
#include <memory>

using Point3f = Eigen::Vector3f;
using Eigen::DiagonalMatrix;
using Eigen::JacobiSVD;
using Eigen::Matrix3f;
using Eigen::Vector3f;

// TODO: check
Eigen::Vector3f PoseEstimator::Points::FindCorrispondence(
    const Eigen::Vector3f& point, const Eigen::VectorXf& descriptor) const {
  float min_dist = sqrt(
      (descriptor - descriptors_2_[0]).dot(descriptor - descriptors_2_[0]));
  Eigen::Vector3f best_association = cloud_2_[0];

  for (size_t i = 1; i < cloud_2_.size(); ++i) {
    float dist = sqrt(
        (descriptor - descriptors_2_[i]).dot(descriptor - descriptors_2_[i]));
    if (dist < min_dist) {
      min_dist = dist;
      best_association = cloud_2_[i];
    }
  }

  return best_association;
};

Estimator::Model* PoseEstimator::Solve(Estimator::Data& data) {
  Points* points = static_cast<Points*>(&data);

  size_t N = points->cloud_2_.size();

  assert(static_cast<int>(N) >= min_obs_solution_);
  assert(N == points->cloud_1_.size());

  Point3f mu_1 = Point3f::Zero(3);
  Point3f mu_2 = Point3f::Zero(3);
  float sigma_1 = 0.f, sigma_2 = 0.f;
  Matrix3f sigma = Matrix3f::Zero(3, 3);
  Matrix3f W = Matrix3f::Identity(3, 3);

  for (size_t i = 0; i < N; ++i) {
    mu_1 += points->cloud_1_[i];
    mu_2 += points->cloud_2_[i];
  }

  mu_1 /= N;
  mu_2 /= N;

  for (size_t i = 0; i < N; ++i) {
    Point3f p_mu_1 = points->cloud_1_[i] - mu_1;
    Point3f p_mu_2 = points->cloud_2_[i] - mu_2;

    sigma_1 += p_mu_1.dot(p_mu_1);
    sigma_2 += p_mu_2.dot(p_mu_2);

    sigma += p_mu_1 * p_mu_2.transpose();
  }

  sigma_1 /= N;
  sigma_2 /= N;
  sigma /= N;

  JacobiSVD<Matrix3f> svd(sigma, Eigen::ComputeFullU | Eigen::ComputeFullV);

  if (svd.matrixU().determinant() * svd.matrixV().determinant() < 0) {
    W = DiagonalMatrix<float, 3>(1, 1, -1);
  }

  Matrix3f R = svd.matrixU() * W * svd.matrixV().transpose();
  float s = 1.f;
  Vector3f t = mu_1 - s * R * mu_2;

  solution_ = std::make_shared<Pose>(R, t);

  return solution_.get();
};

std::tuple<int, std::unique_ptr<Estimator::Data>>
PoseEstimator::EvaluateSolution(Estimator::Data& data,
                                float inliers_threshold) const {
  Points* points = static_cast<Points*>(&data);

  int num_inliers = 0;
  std::unique_ptr<Points> inliers = std::make_unique<Points>();

  if (points->cloud_1_.size() < points->cloud_2_.size()) {
    for (size_t i = 0; i < points->cloud_1_.size(); ++i) {
      Vector3f point = points->cloud_1_[i];
      Vector3f trasformed_point = GetPose()->R().transpose() * point -
                                  GetPose()->R().transpose() * GetPose()->t();

      for (size_t j = 0; j < points->cloud_2_.size(); ++j) {
        Vector3f distance = trasformed_point - points->cloud_2_[j];

        if (sqrt(distance.dot(distance)) < inliers_threshold) {
          num_inliers++;
          inliers->cloud_1_.push_back(point);
          inliers->cloud_2_.push_back(points->cloud_2_[j]);
          break;
        }
      }
    }
  } else {
    for (size_t i = 0; i < points->cloud_2_.size(); ++i) {
      Vector3f point = points->cloud_2_[i];

      Vector3f trasformed_point = GetPose()->R() * point + GetPose()->t();

      for (size_t j = 0; j < points->cloud_1_.size(); ++j) {
        Vector3f distance = trasformed_point - points->cloud_1_[j];

        if (sqrt(distance.dot(distance)) < inliers_threshold) {
          num_inliers++;
          inliers->cloud_1_.push_back(points->cloud_1_[j]);
          inliers->cloud_2_.push_back(point);
          break;
        }
      }
    }
  }

  return std::make_tuple(num_inliers, std::move(inliers));
};

std::tuple<int, std::unique_ptr<Estimator::Data>>
PoseEstimator::SelectBestInliers(Data& data, float inliers_threshold) const {
  Points* points = static_cast<Points*>(&data);

  int num_inliers = 0;
  std::unique_ptr<Points> inliers = std::make_unique<Points>();

  if (points->cloud_1_.size() < points->cloud_2_.size()) {
    for (size_t i = 0; i < points->cloud_1_.size(); ++i) {
      Vector3f point = points->cloud_1_[i];
      Vector3f trasformed_point = GetPose()->R().transpose() * point -
                                  GetPose()->R().transpose() * GetPose()->t();

      Vector3f best_association;
      float best_distance = inliers_threshold;
      bool found = false;

      for (size_t j = 0; j < points->cloud_2_.size(); ++j) {
        float distance = sqrt((trasformed_point - points->cloud_2_[j])
                                  .dot(trasformed_point - points->cloud_2_[j]));

        if (!found && distance < inliers_threshold) {
          found = true;
        }

        if (best_distance > distance) {
          best_distance = distance;
          best_association = points->cloud_2_[j];
        }
      }

      if (found) {
        num_inliers++;
        inliers->cloud_1_.push_back(point);
        inliers->cloud_2_.push_back(best_association);
      }
    }
  } else {
    for (size_t i = 0; i < points->cloud_2_.size(); ++i) {
      Vector3f point = points->cloud_2_[i];

      Vector3f trasformed_point = GetPose()->R() * point + GetPose()->t();

      Vector3f best_association;
      float best_distance = inliers_threshold;
      bool found = false;

      for (size_t j = 0; j < points->cloud_1_.size(); ++j) {
        float distance = sqrt((trasformed_point - points->cloud_1_[j])
                                  .dot(trasformed_point - points->cloud_1_[j]));

        if (!found && distance < inliers_threshold) {
          found = true;
        }

        if (best_distance > distance) {
          best_distance = distance;
          best_association = points->cloud_1_[j];
        }
      }
      if (found) {
        num_inliers++;
        inliers->cloud_1_.push_back(best_association);
        inliers->cloud_2_.push_back(point);
      }
    }
  }

  return std::make_tuple(num_inliers, std::move(inliers));
};

std::unique_ptr<Estimator::Data> PoseEstimator::SelectMinimumSolution(
    Estimator::Data& data) const {
  Points* points = static_cast<Points*>(&data);
  size_t N = points->cloud_1_.size() < points->cloud_2_.size()
                 ? points->cloud_1_.size()
                 : points->cloud_2_.size();

  assert(static_cast<int>(N) >= min_obs_solution_);

  int index = rand() % N;
  int stride = rand() % (N - min_obs_solution_ + 1) + 1;

  std::unique_ptr<Points> min_solution = std::make_unique<Points>();

  for (int _ = 0; _ < min_obs_solution_; ++_) {
    min_solution->cloud_1_.push_back(points->cloud_1_[index]);

    min_solution->cloud_2_.push_back(points->FindCorrispondence(
        points->cloud_1_[index], points->descriptors_1_[index]));

    index = (index + stride) % N;
  }

  return std::move(min_solution);
};
