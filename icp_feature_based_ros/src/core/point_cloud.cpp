#include "point_cloud.hpp"

template <typename pT, typename iT>
PointCloud<pT, iT>::PointCloud(size_t heigh, size_t width)
    : x_(heigh * width),
      y_(heigh * width),
      z_(heigh * width),
      i_(heigh * width),
      h_(heigh),
      w_(width){};

template <typename pT, typename iT>
PointCloud<pT, iT>::PointCloud(size_t dim)
    : x_(dim), y_(dim), z_(dim), i_(dim), h_(1), w_(dim){};

template <typename pT, typename iT>
PointCloud<pT, iT>::PointCloud() : x_(0), y_(0), z_(0), i_(0), h_(0), w_(0){};

template <typename pT, typename iT>
struct PointCloud<pT, iT>::Point3DI PointCloud<pT, iT>::p(uint index) const {
  Point3DI point = {x_(index), y_(index), z_(index), i_(index)};
  return point;
};

template PointCloudf::PointCloud(size_t heigh, size_t width);

template PointCloudf::PointCloud(size_t dim);

template PointCloudf::PointCloud();

// template Point3DIf PointCloudf::p(uint index);
