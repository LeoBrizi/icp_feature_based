#include "sparse_registrator.hpp"

#include <sensor_msgs/point_cloud2_iterator.h>

#include <Eigen/StdVector>
#include <iostream>
#include <opencv2/core.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/highgui.hpp>
#include <tuple>

using cv::KeyPoint;
using cv::Mat;
using std::make_tuple;
using std::make_unique;
using std::tuple;

SparseRegistrator::SparseRegistrator(SuperPointDetector& sp_detector,
                                     LidarProjector& lidar_projector,
                                     int ransac_iterations,
                                     float inliers_threshold)
    : sp_detector_{sp_detector}, lidar_projector_{lidar_projector} {
  estimator_ = make_unique<PoseEstimator>();
  ransac_ = make_unique<Ransac>(*(estimator_.get()), ransac_iterations,
                                inliers_threshold);
};

SparseRegistrator::SparseRegistrator(SuperPointDetector& sp_detector,
                                     LidarProjector& lidar_projector,
                                     int ransac_iterations,
                                     float inliers_threshold, bool show_image)
    : sp_detector_{sp_detector},
      lidar_projector_{lidar_projector},
      show_image_(show_image) {
  estimator_ = make_unique<PoseEstimator>();
  ransac_ = make_unique<Ransac>(*(estimator_.get()), ransac_iterations,
                                inliers_threshold);
};

tuple<bool, PoseEstimator::Pose*> SparseRegistrator::Register(
    PointCloudf& prev_cloud, PointCloudf& curr_cloud) {
  std::vector<KeyPoint> prev_keypoints, curr_keypoints;
  Mat prev_descriptors, curr_descriptors;

  auto prev_image = lidar_projector_.Project2ImageAndNormalize(prev_cloud);
  auto curr_image = lidar_projector_.Project2ImageAndNormalize(curr_cloud);

  sp_detector_.detectAndCompute(prev_image->Convert4SP()->intensity_,
                                prev_keypoints, prev_descriptors);
  sp_detector_.detectAndCompute(curr_image->Convert4SP()->intensity_,
                                curr_keypoints, curr_descriptors);

  std::vector<Point, Eigen::aligned_allocator<Point>> cloud_1, cloud_2;
  std::vector<Descriptor, Eigen::aligned_allocator<Descriptor>> desc_1, desc_2;

  for (size_t i = 0; i < prev_keypoints.size(); ++i) {
    cloud_1.push_back(
        prev_image->GetPoint(prev_keypoints[i].pt.y, prev_keypoints[i].pt.x));

    Eigen::Map<Eigen::Matrix<float, DESCRIPTOR_SIZE, 1>> descriptor(
        prev_descriptors.row(i).ptr<float>(), DESCRIPTOR_SIZE, 1);

    desc_1.push_back(descriptor);
  }

  for (size_t i = 0; i < curr_keypoints.size(); ++i) {
    cloud_2.push_back(
        prev_image->GetPoint(curr_keypoints[i].pt.y, curr_keypoints[i].pt.x));

    Eigen::Map<Eigen::Matrix<float, DESCRIPTOR_SIZE, 1>> descriptor(
        curr_descriptors.row(i).ptr<float>(), DESCRIPTOR_SIZE, 1);

    desc_2.push_back(descriptor);
  }

  PoseEstimator::Points input_data(cloud_1, desc_1, cloud_2, desc_2);

  auto [find, best_sol] = ransac_->FindBestSolution(input_data);

  return make_tuple(find, static_cast<PoseEstimator::Pose*>(best_sol));
};

// FASTER than the one before, consider using this for a stream of clouds
tuple<bool, PoseEstimator::Pose*> SparseRegistrator::Register(
    PointCloudf& new_cloud) {
  std::vector<KeyPoint> keypoints;
  Mat descriptors;

  auto image = lidar_projector_.Project2ImageAndNormalize(new_cloud);

  auto cv_image = image->Convert4SP();

  sp_detector_.detectAndCompute(cv_image->intensity_, keypoints, descriptors);

  std::vector<Point, Eigen::aligned_allocator<Point>> new_points;
  std::vector<Descriptor, Eigen::aligned_allocator<Descriptor>> new_descriptors;

  for (size_t i = 0; i < keypoints.size(); ++i) {
    Point point = image->GetPoint(keypoints[i].pt.y, keypoints[i].pt.x);

    if (point.isZero()) continue;

    new_points.push_back(point);

    Eigen::Map<Eigen::Matrix<float, DESCRIPTOR_SIZE, 1>> descriptor(
        descriptors.row(i).ptr<float>(), DESCRIPTOR_SIZE, 1);

    new_descriptors.push_back(descriptor);
  }

  if (prev_points_.size() < 3) {
    prev_points_ = new_points;
    prev_descriptors_ = new_descriptors;
    return make_tuple(false, nullptr);
  }
  if (new_points.size() < 3) {
    return make_tuple(false, nullptr);
  }

  PoseEstimator::Points input_data(prev_points_, prev_descriptors_, new_points,
                                   new_descriptors);

  // auto [find, best_sol] = ransac_->FindBestSolution(input_data);

  auto [find, best_sol, inliers] = ransac_->FindBestSolutionDebug(input_data);

  prev_points_ = new_points;
  prev_descriptors_ = new_descriptors;
  inliers_ = std::move(inliers);

  return make_tuple(find, static_cast<PoseEstimator::Pose*>(best_sol));
};

tuple<bool, PoseEstimator::Pose*> SparseRegistrator::RegisterDebug(
    PointCloudf& prev_cloud, PointCloudf& curr_cloud) {
  std::vector<KeyPoint> prev_keypoints, curr_keypoints;
  Mat prev_descriptors, curr_descriptors;

  auto prev_image = lidar_projector_.Project2ImageAndNormalize(prev_cloud);
  auto curr_image = lidar_projector_.Project2ImageAndNormalize(curr_cloud);

  sp_detector_.detectAndCompute(prev_image->Convert4SP()->intensity_,
                                prev_keypoints, prev_descriptors);
  sp_detector_.detectAndCompute(curr_image->Convert4SP()->intensity_,
                                curr_keypoints, curr_descriptors);

  cv::Mat prev_sp_intensity;
  cv::cvtColor(prev_image->Convert4SP()->intensity_, prev_sp_intensity,
               cv::COLOR_GRAY2RGB);
  for (const auto& keypoint : prev_keypoints) {
    cv::circle(prev_sp_intensity, keypoint.pt, 2.0, cv::Scalar(1.f, 0, 0), -1);
    cv::circle(prev_sp_intensity, keypoint.pt, keypoint.size,
               cv::Scalar(0.1f, 1.f, 0.1f), 1);
  }

  cv::imshow("Intensity1", prev_sp_intensity);
  cv::waitKey(3);

  cv::Mat curr_sp_intensity;
  cv::cvtColor(curr_image->Convert4SP()->intensity_, curr_sp_intensity,
               cv::COLOR_GRAY2RGB);
  for (const auto& keypoint : curr_keypoints) {
    cv::circle(curr_sp_intensity, keypoint.pt, 2.0, cv::Scalar(1.f, 0, 0), -1);
    cv::circle(curr_sp_intensity, keypoint.pt, keypoint.size,
               cv::Scalar(0.1f, 1.f, 0.1f), 1);
  }

  cv::imshow("Intensity2", curr_sp_intensity);
  cv::waitKey(3);

  std::vector<Point, Eigen::aligned_allocator<Point>> cloud_1, cloud_2;
  std::vector<Descriptor, Eigen::aligned_allocator<Descriptor>> desc_1, desc_2;

  for (size_t i = 0; i < prev_keypoints.size(); ++i) {
    cloud_1.push_back(
        prev_image->GetPoint(prev_keypoints[i].pt.y, prev_keypoints[i].pt.x));

    Eigen::Map<Eigen::Matrix<float, DESCRIPTOR_SIZE, 1>> descriptor(
        prev_descriptors.row(i).ptr<float>(), DESCRIPTOR_SIZE, 1);

    desc_1.push_back(descriptor);
  }

  for (size_t i = 0; i < curr_keypoints.size(); ++i) {
    cloud_2.push_back(
        prev_image->GetPoint(curr_keypoints[i].pt.y, curr_keypoints[i].pt.x));

    Eigen::Map<Eigen::Matrix<float, DESCRIPTOR_SIZE, 1>> descriptor(
        curr_descriptors.row(i).ptr<float>(), DESCRIPTOR_SIZE, 1);

    desc_2.push_back(descriptor);
  }

  PoseEstimator::Points input_data(cloud_1, desc_1, cloud_2, desc_2);

  auto [find, best_sol, associations] =
      ransac_->FindBestSolutionDebug(input_data);

  if (associations) {
    PoseEstimator::Points* solution_points =
        static_cast<PoseEstimator::Points*>(associations.get());

    std::vector<cv::KeyPoint> keypoints_1, keypoints_2;
    std::vector<cv::DMatch> matches;

    auto prev_inliers =
        lidar_projector_.ProjectPoints(solution_points->cloud_1_);

    auto curr_inliers =
        lidar_projector_.ProjectPoints(solution_points->cloud_2_);

    for (size_t i = 0; i < curr_inliers.size(); ++i) {
      keypoints_1.push_back(
          cv::KeyPoint(prev_inliers[i][0], prev_inliers[i][1], 4));

      keypoints_2.push_back(
          cv::KeyPoint(curr_inliers[i][0], curr_inliers[i][1], 4));

      matches.push_back(cv::DMatch(i, i, 0));
    }

    cv::Mat out_image, frame_1, frame_2;

    frame_1 = prev_image->Convert4SP()->intensity_ * 255;
    frame_2 = curr_image->Convert4SP()->intensity_ * 255;

    // frame_1.convertTo(frame_1, CV_8U);
    // frame_2.convertTo(frame_2, CV_8U);

    // cv::cvtColor(frame_1, frame_1, cv::COLOR_GRAY2RGB);
    // cv::cvtColor(frame_2, frame_2, cv::COLOR_GRAY2RGB);

    // cv::drawMatches(frame_1, keypoints_1, frame_2, keypoints_2, matches,
    //                 out_image, cv::Scalar::all(-1), cv::Scalar::all(-1),
    //                 std::vector<char>(),
    //                 cv::DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);

    out_image = cv::Mat(128, 1024, CV_32FC1);
    frame_1.copyTo(out_image(cv::Rect(0, 0, 1024, 64)));
    frame_2.copyTo(out_image(cv::Rect(0, 64, 1024, 64)));

    out_image.convertTo(out_image, CV_8U);
    cv::cvtColor(out_image, out_image, cv::COLOR_GRAY2RGB);

    for (const auto& keypoint : prev_keypoints) {
      cv::circle(out_image, keypoint.pt, 3.0, cv::Scalar(255, 0, 0), -1);
    }
    for (const auto& keypoint : curr_keypoints) {
      cv::circle(out_image, cv::Point(keypoint.pt.x, keypoint.pt.y + 64), 3.0,
                 cv::Scalar(255, 0, 0), -1);
    }
    for (const auto& match : matches) {
      cv::circle(out_image, keypoints_1[match.trainIdx].pt, 3.0,
                 cv::Scalar(0, 255, 0), -1);
      cv::circle(out_image,
                 cv::Point(keypoints_2[match.queryIdx].pt.x,
                           keypoints_2[match.queryIdx].pt.y + 64),
                 3.0, cv::Scalar(0, 255, 0), -1);
      cv::line(out_image, keypoints_1[match.trainIdx].pt,
               cv::Point(keypoints_2[match.queryIdx].pt.x,
                         keypoints_2[match.queryIdx].pt.y + 64),
               cv::Scalar(0, 255, 0), 1);
    }

    //-- Show detected matches
    imshow("Inliers Matches", out_image);
    cv::waitKey(3);
  }

  return make_tuple(find, static_cast<PoseEstimator::Pose*>(best_sol));
};

sensor_msgs::PointCloud2 SparseRegistrator::GetFeaturesInliersCloud() const {
  sensor_msgs::PointCloud2 features_cloud;

  PoseEstimator::Points* inliers =
      static_cast<PoseEstimator::Points*>(inliers_.get());

  features_cloud.width = inliers->cloud_2_.size();
  features_cloud.height = 1;
  // for fields setup
  sensor_msgs::PointCloud2Modifier modifier(features_cloud);
  modifier.setPointCloud2Fields(3, "x", 1, sensor_msgs::PointField::FLOAT32,
                                "y", 1, sensor_msgs::PointField::FLOAT32, "z",
                                1, sensor_msgs::PointField::FLOAT32);
  modifier.resize(inliers->cloud_2_.size());

  sensor_msgs::PointCloud2Iterator<float> out_x(features_cloud, "x");
  sensor_msgs::PointCloud2Iterator<float> out_y(features_cloud, "y");
  sensor_msgs::PointCloud2Iterator<float> out_z(features_cloud, "z");

  for (size_t i = 0; i < inliers->cloud_2_.size(); ++i) {
    *out_x = inliers->cloud_2_[i][0];
    *out_y = inliers->cloud_2_[i][1];
    *out_z = inliers->cloud_2_[i][2];
    ++out_x;
    ++out_y;
    ++out_z;
  }

  features_cloud.is_bigendian = false;
  features_cloud.is_dense = false;  // there may be invalid points

  return features_cloud;
};

sensor_msgs::PointCloud2 SparseRegistrator::GetFeaturesCloud() const {
  sensor_msgs::PointCloud2 features_cloud;

  features_cloud.width = prev_points_.size();
  features_cloud.height = 1;
  // for fields setup
  sensor_msgs::PointCloud2Modifier modifier(features_cloud);
  modifier.setPointCloud2Fields(3, "x", 1, sensor_msgs::PointField::FLOAT32,
                                "y", 1, sensor_msgs::PointField::FLOAT32, "z",
                                1, sensor_msgs::PointField::FLOAT32);
  modifier.resize(prev_points_.size());

  sensor_msgs::PointCloud2Iterator<float> out_x(features_cloud, "x");
  sensor_msgs::PointCloud2Iterator<float> out_y(features_cloud, "y");
  sensor_msgs::PointCloud2Iterator<float> out_z(features_cloud, "z");

  for (size_t i = 0; i < prev_points_.size(); ++i) {
    *out_x = prev_points_[i][0];
    *out_y = prev_points_[i][1];
    *out_z = prev_points_[i][2];
    ++out_x;
    ++out_y;
    ++out_z;
  }

  features_cloud.is_bigendian = false;
  features_cloud.is_dense = false;  // there may be invalid points

  return features_cloud;
};
