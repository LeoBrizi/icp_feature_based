#pragma once

#include <sensor_msgs/PointCloud2.h>

#include <Eigen/Dense>
#include <Eigen/StdVector>
#include <core/point_cloud.hpp>
#include <core/pose_estimator.hpp>
#include <core/ransac.hpp>
#include <memory>
#include <preprocessing/lidar_projector.hpp>
#include <preprocessing/superpoint.hpp>

#define DESCRIPTOR_SIZE 256

using Descriptor = Eigen::VectorXf;
using Point = Eigen::Vector3f;

class SparseRegistrator {
 public:
  explicit SparseRegistrator(SuperPointDetector& sp_detector,
                             LidarProjector& lidar_projector,
                             int ransac_iterations, float inliers_threshold);

  explicit SparseRegistrator(SuperPointDetector& sp_detector,
                             LidarProjector& lidar_projector,
                             int ransac_iterations, float inliers_threshold,
                             bool show_image);

  std::tuple<bool, PoseEstimator::Pose*> Register(PointCloudf& prev_cloud,
                                                  PointCloudf& curr_cloud);

  std::tuple<bool, PoseEstimator::Pose*> Register(PointCloudf& new_cloud);

  std::tuple<bool, PoseEstimator::Pose*> RegisterDebug(PointCloudf& prev_cloud,
                                                       PointCloudf& curr_cloud);

  sensor_msgs::PointCloud2 GetFeaturesCloud() const;
  sensor_msgs::PointCloud2 GetFeaturesInliersCloud() const;

 private:
  void DebugDrawing();

  std::unique_ptr<PoseEstimator> estimator_;
  std::unique_ptr<Ransac> ransac_;
  SuperPointDetector& sp_detector_;
  LidarProjector& lidar_projector_;

  bool show_image_ = false;

  std::vector<Point, Eigen::aligned_allocator<Point>> prev_points_;
  std::vector<Descriptor, Eigen::aligned_allocator<Descriptor>>
      prev_descriptors_;

  std::unique_ptr<Estimator::Data> inliers_;
};