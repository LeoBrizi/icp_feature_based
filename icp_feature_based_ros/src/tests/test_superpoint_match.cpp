#include <opencv2/core.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/highgui.hpp>
#include <preprocessing/superpoint.hpp>
#include <vector>

int main(int argc, char** argv) {
  cv::Mat frame_1, frame_2, out_image;

  SuperPointDetector sp_detector = SuperPointDetector(-1, 0.15f);
  std::vector<cv::KeyPoint> keypoints_1, keypoints_2;
  cv::Mat descriptors_1, descriptors_2;

  frame_1 = cv::Mat::ones(cv::Size(640, 480), CV_32FC1) / 2.f;
  frame_2 = cv::Mat::ones(cv::Size(640, 480), CV_32FC1) / 2.f;

  cv::Point square_center(640 / 2, 480 / 2);
  int square_side = 150;

  for (int r = square_center.y - square_side / 2;
       r < square_center.y + square_side / 2; ++r)
    for (int c = square_center.x - square_side / 2;
         c < square_center.x + square_side / 2; ++c) {
      frame_1.at<float>(r, c) = 1.f;
      frame_2.at<float>(r + 100, c + 100) = 1.f;
    }

  sp_detector.detectAndCompute(frame_1, keypoints_1, descriptors_1);
  sp_detector.detectAndCompute(frame_2, keypoints_2, descriptors_2);

  cv::cvtColor(frame_1, out_image, cv::COLOR_GRAY2RGB);
  for (const auto& keypoint : keypoints_1) {
    cv::circle(out_image, keypoint.pt, 2.0, cv::Scalar(1.f, 0, 0), -1);
    cv::circle(out_image, keypoint.pt, keypoint.size,
               cv::Scalar(0.1f, 1.f, 0.1f), 1);
  }

  cv::imshow("frame 1", out_image);

  cv::cvtColor(frame_2, out_image, cv::COLOR_GRAY2RGB);
  for (const auto& keypoint : keypoints_2) {
    cv::circle(out_image, keypoint.pt, 2.0, cv::Scalar(1.f, 0, 0), -1);
    cv::circle(out_image, keypoint.pt, keypoint.size,
               cv::Scalar(0.1f, 1.f, 0.1f), 1);
  }

  cv::imshow("frame 2", out_image);
  cv::waitKey();

  //-- Step 2: Matching descriptor vectors with a FLANN based matcher
  cv::Ptr<cv::DescriptorMatcher> matcher =
      cv::DescriptorMatcher::create("FlannBased");

  std::vector<std::vector<cv::DMatch>> knn_matches;
  matcher->knnMatch(descriptors_1, descriptors_2, knn_matches, 2);

  //-- Filter matches using the Lowe's ratio test
  const float ratio_thresh = 0.7f;
  std::vector<cv::DMatch> good_matches;
  for (size_t i = 0; i < knn_matches.size(); ++i) {
    if (knn_matches[i][0].distance <
        ratio_thresh * knn_matches[i][1].distance) {
      good_matches.push_back(knn_matches[i][0]);
    }
  }

  frame_1 *= 255;
  frame_2 *= 255;
  frame_1.convertTo(frame_1, CV_8U);
  frame_2.convertTo(frame_2, CV_8U);
  cv::cvtColor(frame_1, frame_1, cv::COLOR_GRAY2RGB);
  cv::cvtColor(frame_2, frame_2, cv::COLOR_GRAY2RGB);

  //-- Draw matches
  cv::drawMatches(frame_1, keypoints_1, frame_2, keypoints_2, good_matches,
                  out_image, cv::Scalar::all(-1), cv::Scalar::all(-1),
                  std::vector<char>(),
                  cv::DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);

  //-- Show detected matches
  imshow("Good Matches", out_image);
  cv::waitKey();
  exit(0);
}
