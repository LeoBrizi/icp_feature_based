#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <preprocessing/superpoint.hpp>
#include <vector>

int main(int argc, char** argv) {
  cv::Mat frame, out_image;

  SuperPointDetector sp_detector = SuperPointDetector();
  std::vector<cv::KeyPoint> keypoints;

  frame = cv::Mat::ones(cv::Size(640, 480), CV_32FC1) / 2.f;

  cv::Point square_center(640 / 2, 480 / 2);
  int square_side = 150;

  for (int r = square_center.y - square_side / 2;
       r < square_center.y + square_side / 2; ++r)
    for (int c = square_center.x - square_side / 2;
         c < square_center.x + square_side / 2; ++c)
      frame.at<float>(r, c) = 1.f;

  sp_detector.detect(frame, keypoints);

  cv::cvtColor(frame, out_image, cv::COLOR_GRAY2RGB);
  for (const auto& keypoint : keypoints) {
    cv::circle(out_image, keypoint.pt, 2.0, cv::Scalar(1.f, 0, 0), -1);
    cv::circle(out_image, keypoint.pt, keypoint.size,
               cv::Scalar(0.1f, 1.f, 0.1f), 1);
  }
  cv::imshow("superpoints_extraction", out_image);
  cv::waitKey();
  exit(0);
}
