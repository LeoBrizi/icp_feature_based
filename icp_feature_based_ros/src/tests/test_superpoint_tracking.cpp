#include <opencv2/core.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/highgui.hpp>
#include <preprocessing/superpoint.hpp>
#include <tracking/point_tracker.hpp>
#include <vector>

int main(int argc, char** argv) {
  cv::Point square_center(640 / 2 - 200, 480 / 2 - 100);
  cv::Point square_velocity(20, 10);
  int square_side = 100;

  int num_of_frame = 20;
  std::vector<cv::Mat> frames;

  for (int i = 0; i < num_of_frame; ++i) {
    cv::Mat new_frame = cv::Mat::ones(cv::Size(640, 480), CV_32FC1) / 2.f;
    cv::Point current_center = square_center + square_velocity * i;

    for (int r = current_center.y - square_side / 2;
         r < current_center.y + square_side / 2; ++r)
      for (int c = current_center.x - square_side / 2;
           c < current_center.x + square_side / 2; ++c) {
        new_frame.at<float>(r, c) = 1.f;
      }

    frames.push_back(new_frame);
  }

  SuperPointDetector sp_detector = SuperPointDetector(-1, 0.15f);
  std::vector<cv::KeyPoint> keypoints;
  cv::Mat descriptors;

  PointTracker point_tracker = PointTracker(Matcher::FLANNMatcher);

  cv::Mat out_image;
  for (int i = 0; i < num_of_frame; ++i) {
    sp_detector.detectAndCompute(frames[i], keypoints, descriptors);

    point_tracker.Update(keypoints, descriptors);

    cv::cvtColor(frames[i], out_image, cv::COLOR_GRAY2RGB);
    point_tracker.DrawTracks<CV_32F>(out_image);

    imshow("Superpoint tracking", out_image);
    cv::waitKey(10);
  }
  exit(0);
}
