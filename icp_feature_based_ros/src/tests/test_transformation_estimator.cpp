#include <Eigen/Core>
#include <Eigen/Geometry>
#include <Eigen/StdVector>
#include <core/pose_estimator.hpp>
#include <iostream>

int main(int argc, char** argv) {
  Eigen::Matrix3f R = Eigen::Quaternionf::UnitRandom().toRotationMatrix();
  Eigen::Vector3f t;
  t << 1, 2, 3;

  std::vector<Eigen::Vector3f, Eigen::aligned_allocator<Eigen::Vector3f>>
      cloud_1, cloud_2;

  for (uint i = 0; i < 3; ++i) {
    Eigen::Vector3f new_point = 10 * Eigen::Vector3f::Random(3);
    cloud_1.push_back(new_point);
    cloud_2.push_back(R * new_point + t);
  }

  PoseEstimator estimator = PoseEstimator();
  PoseEstimator::Points input_data(cloud_1, cloud_2);
  estimator.Solve(input_data);

  Eigen::Matrix4f H = Eigen::Matrix4f::Identity(4, 4);
  H.block<3, 3>(0, 0) = R;
  H.block<3, 1>(0, 3) = t;

  auto [score, _] = estimator.EvaluateSolution(input_data, 1e-5);

  std::cout << H * estimator.GetPose()->H() << std::endl;
  std::cout << "number of inliers: " << score << "/3" << std::endl;
  if ((H * estimator.GetPose()->H()).isIdentity(0.001)) {
    std::cout << "IT WORKS :)\n";
    exit(0);
  } else {
    exit(1);
  }
}