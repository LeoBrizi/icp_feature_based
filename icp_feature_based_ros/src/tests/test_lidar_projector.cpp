
#include <core/point_cloud.hpp>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <preprocessing/lidar_projector.hpp>

int main(int argc, char** argv) {
  float height = 64;
  float width = 1024;
  float fov_up = 16.6;
  float fov_down = -16.6;

  PointCloudf cloud(height, width);

  float radius = 4.f;

  float gamma = fov_up;
  for (uint row = 0; row < height; ++row) {
    float alpha = 0.f;

    for (uint col = 0; col < width; ++col) {
      uint index = row * width + col;

      cloud.x(index) = radius * std::cos((alpha * M_PI) / 180.f);
      cloud.y(index) = radius * std::sin((alpha * M_PI) / 180.f);
      cloud.z(index) = radius * std::tan((gamma * M_PI) / 180.f);

      cloud.i(index) = 1.f;
      alpha += 360.f / width;
    }

    gamma -= (std::abs(fov_up) + std::abs(fov_down)) / height;
  }

  LidarProjector projector(height, width, fov_up, fov_down);

  auto image = projector.Project2Image(cloud);

  auto cv_image = image->Convert2CvImage<CV_8U>();

  cv::imshow("Depth", cv_image->depth_);
  cv::waitKey();
  cv::imshow("Intensity", cv_image->intensity_);
  cv::waitKey();

  exit(0);
}
