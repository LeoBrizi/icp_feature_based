#pragma once

#include <Eigen/Dense>
#include <Eigen/StdVector>
#include <core/point_cloud.hpp>
#include <memory>
#include <opencv2/opencv.hpp>

#define INVALID_VALUE -0.1

typedef struct {
  cv::Mat depth_;
  cv::Mat intensity_;
} CvImage;

class Image {
 public:
  Image(uint rows, uint cols);

  template <int I>
  std::shared_ptr<CvImage> Convert2CvImage(float max_depth = 0.f,
                                           float max_intensity = 0.f) const;

  std::shared_ptr<CvImage> Convert4SP() const;

  inline const uint& rows() { return this->rows_; };
  inline const uint& cols() { return this->cols_; };

  inline Eigen::Vector3f GetPoint(int r, int c) {
    // Eigen::Array3f point = this->xyz_.row((r * cols_) + c);
    // return Eigen::Vector3f(point);

    Eigen::Vector3f point;
    point[0] = this->xyz_[((r * cols_) + c) * 3];
    point[1] = this->xyz_[((r * cols_) + c) * 3 + 1];
    point[2] = this->xyz_[((r * cols_) + c) * 3 + 2];

    return point;
  };

 private:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  // Eigen::Array<float, Eigen::Dynamic, 3> xyz_;
  // std::vector<Eigen::Vector3f, Eigen::aligned_allocator<Eigen::Vector3f>>
  // xyz_;
  Eigen::Array<float, Eigen::Dynamic, 1> xyz_;
  Eigen::Array<float, Eigen::Dynamic, 1> depth_;
  Eigen::Array<float, Eigen::Dynamic, 1> intensity_;

  uint rows_;
  uint cols_;
  friend class LidarProjector;
};

class LidarProjector {
 public:
  explicit LidarProjector(uint height, uint width, float fov_up,
                          float fov_down);

  explicit LidarProjector(uint height, uint width, float fov_up, float fov_down,
                          float max_depth, float max_intensity);

  std::shared_ptr<Image> Project2Image(PointCloudf& cloud) const;

  std::shared_ptr<Image> Project2ImageAndNormalize(PointCloudf& cloud) const;

  std::vector<Eigen::Vector2i, Eigen::aligned_allocator<Eigen::Vector2i>>
  ProjectPoints(const std::vector<Eigen::Vector3f,
                                  Eigen::aligned_allocator<Eigen::Vector3f>>&
                    points) const;

 private:
  uint height_;
  uint width_;
  float fov_up_;
  float fov_down_;
  float max_depth_ = 0.f;
  float max_intensity_ = 0.f;
};
