#include "lidar_projector.hpp"

#include <math.h>
#include <omp.h>

#include <utilities/cv_types.hpp>

#include "assert.h"

using Eigen::Array3f;
using Eigen::ArrayXf;
using Eigen::ArrayXi;
using Eigen::Dynamic;
using Eigen::Map;
using Eigen::Matrix;
using Eigen::Vector3f;
using std::make_shared;
using std::shared_ptr;

Image::Image(uint rows, uint cols) : rows_(rows), cols_(cols) {
  // xyz_ = Eigen::Array<float, Eigen::Dynamic, 3>::Zero(rows * cols, 3);
  // xyz_.reserve(rows * cols);
  xyz_ = Eigen::ArrayXf::Zero(rows * cols * 3);
  depth_ = ArrayXf::Ones(rows * cols) * INVALID_VALUE;
  intensity_ = ArrayXf::Ones(rows * cols) * INVALID_VALUE;
};

// TODO: convert only the requested image
template <int I>
shared_ptr<CvImage> Image::Convert2CvImage(float max_depth,
                                           float max_intensity) const {
  shared_ptr<CvImage> cv_image = make_shared<CvImage>();

  float max_depth_ = max_depth;
  if (max_depth_ < 0.1f) {
    max_depth_ = depth_.maxCoeff();
  }

  ArrayXf norm_intensity;
  if (max_intensity < 0.1f) {
    float max_intensity_ = intensity_.maxCoeff();
    norm_intensity = (intensity_ / max_intensity_) * CvType<I>::max_val;
  } else {
    norm_intensity = intensity_.unaryExpr(
        [&](const float& x) { return x > max_intensity ? max_intensity : x; });
    norm_intensity = (norm_intensity / max_intensity) * CvType<I>::max_val;
  }

  ArrayXf norm_depth = (depth_ / max_depth_) * CvType<I>::max_val;
  cv_image->depth_ = cv::Mat(rows_, cols_, I);

  cv_image->intensity_ = cv::Mat(rows_, cols_, I);

#pragma omp parallel for
  for (int r = 0; r < cv_image->depth_.rows; ++r) {
    for (int c = 0; c < cv_image->depth_.cols; ++c) {
      cv_image->depth_.at<typename CvType<I>::type_t>(r, c) =
          static_cast<typename CvType<I>::type_t>(norm_depth(r * cols_ + c));
      cv_image->intensity_.at<typename CvType<I>::type_t>(r, c) =
          static_cast<typename CvType<I>::type_t>(
              norm_intensity(r * cols_ + c));
    }
  }

  return cv_image;
};

template shared_ptr<CvImage> Image::Convert2CvImage<CV_8U>(
    float max_depth, float max_intensity) const;

template shared_ptr<CvImage> Image::Convert2CvImage<CV_32F>(
    float max_depth, float max_intensity) const;

template shared_ptr<CvImage> Image::Convert2CvImage<CV_64F>(
    float max_depth, float max_intensity) const;

shared_ptr<CvImage> Image::Convert4SP() const {
  shared_ptr<CvImage> cv_image = make_shared<CvImage>();

  cv_image->depth_ = cv::Mat(rows_, cols_, CV_32F);

  cv_image->intensity_ = cv::Mat(rows_, cols_, CV_32F);

#pragma omp parallel for
  for (int r = 0; r < cv_image->depth_.rows; ++r) {
    for (int c = 0; c < cv_image->depth_.cols; ++c) {
      cv_image->depth_.at<float>(r, c) =
          static_cast<float>(depth_(r * cols_ + c));
      cv_image->intensity_.at<float>(r, c) =
          static_cast<float>(intensity_(r * cols_ + c));
    }
  }

  return cv_image;
};

LidarProjector::LidarProjector(uint height, uint width, float fov_up,
                               float fov_down)
    : height_(height), width_(width), fov_up_(fov_up), fov_down_(fov_down){};

LidarProjector::LidarProjector(uint height, uint width, float fov_up,
                               float fov_down, float max_depth,
                               float max_intensity)
    : height_(height),
      width_(width),
      fov_up_(fov_up),
      fov_down_(fov_down),
      max_depth_(max_depth),
      max_intensity_(max_intensity){};

inline ArrayXi ToIntAndClamp(const int& min, const int& max,
                             const ArrayXf& array) {
  ArrayXf res = array.round();
  res = res.unaryExpr([&](const float& x) { return std::min<float>(max, x); });
  res = res.unaryExpr([&](const float& x) { return std::max<float>(min, x); });
  return res.cast<int>();
}

shared_ptr<Image> LidarProjector::Project2Image(PointCloudf& cloud) const {
  shared_ptr<Image> image = make_shared<Image>(height_, width_);

  float fov_up = (fov_up_ * M_PI) / 180.f;
  float fov_down = (fov_down_ * M_PI) / 180.f;
  float fov = std::abs(fov_down) + std::abs(fov_up);

  ArrayXf depth = cloud.x().square() + cloud.y().square() + cloud.z().square();
  depth = depth.sqrt();

  ArrayXf yaw = cloud.y().binaryExpr(
      cloud.x(),
      [](const float& y, const float& x) { return std::atan2(y, x); });

  ArrayXf pitch = cloud.z() / depth;
  pitch = pitch.asin();

  ArrayXf proj_x = (0.5f * (1.f - (yaw / M_PI))) * float(width_);
  ArrayXf proj_y = (1.f - ((pitch + fov_up) / fov)) * float(height_);

  ArrayXi u = ToIntAndClamp(0, width_ - 1, proj_x);
  ArrayXi v = ToIntAndClamp(0, height_ - 1, proj_y);

  // TODO: try to optimize this for
  uint index;
  for (uint i = 0; i < width_ * height_; ++i) {
    index = (v(i) * width_) + u(i);
    if (image->depth_(index) < 0.1 || image->depth_(index) > depth(i)) {
      image->depth_(index) = depth(i);
      image->intensity_(index) = cloud.i(i);
      // image->xyz_.row(index) = Array3f(cloud.x(i), cloud.y(i), cloud.z(i));
      // image->xyz_[index] = Eigen::Vector3f(cloud.x(i), cloud.y(i),
      // cloud.z(i));
      image->xyz_[index * 3] = cloud.x(i);
      image->xyz_[index * 3 + 1] = cloud.y(i);
      image->xyz_[index * 3 + 2] = cloud.z(i);
    }
  }

  return image;
};

shared_ptr<Image> LidarProjector::Project2ImageAndNormalize(
    PointCloudf& cloud) const {
  assert(max_depth_ > 0.1f);
  assert(max_intensity_ > 0.1f);

  shared_ptr<Image> image = make_shared<Image>(height_, width_);

  float fov_up = (fov_up_ * M_PI) / 180.f;
  float fov_down = (fov_down_ * M_PI) / 180.f;
  float fov = std::abs(fov_down) + std::abs(fov_up);

  ArrayXf depth = cloud.x().square() + cloud.y().square() + cloud.z().square();
  depth = depth.sqrt();

  ArrayXf yaw = cloud.y().binaryExpr(
      cloud.x(),
      [](const float& y, const float& x) { return std::atan2(y, x); });

  ArrayXf pitch = cloud.z() / depth;
  pitch = pitch.asin();

  ArrayXf proj_x = (0.5f * (1.f - (yaw / M_PI))) * float(width_);
  ArrayXf proj_y = (1.f - ((pitch + fov_up) / fov)) * float(height_);

  ArrayXi u = ToIntAndClamp(0, width_ - 1, proj_x);
  ArrayXi v = ToIntAndClamp(0, height_ - 1, proj_y);

  depth /= max_depth_;

  // TODO: try to optimize this for
  uint index;
  for (uint i = 0; i < width_ * height_; ++i) {
    index = (v(i) * width_) + u(i);

    if (image->depth_(index) < 0.1 || image->depth_(index) > depth(i)) {
      image->depth_(index) = depth(i);
      image->intensity_(index) =
          cloud.i(i) / max_intensity_ > 1 ? 1 : cloud.i(i) / max_intensity_;
      // image->xyz_.row(index) = Array3f(cloud.x(i), cloud.y(i), cloud.z(i));
      // image->xyz_[index] = Eigen::Vector3f(cloud.x(i), cloud.y(i),
      // cloud.z(i));
      image->xyz_[index * 3] = cloud.x(i);
      image->xyz_[index * 3 + 1] = cloud.y(i);
      image->xyz_[index * 3 + 2] = cloud.z(i);
    }
  }

  return image;
};

std::vector<Eigen::Vector2i, Eigen::aligned_allocator<Eigen::Vector2i>>
LidarProjector::ProjectPoints(
    const std::vector<Eigen::Vector3f,
                      Eigen::aligned_allocator<Eigen::Vector3f>>& points)
    const {
  std::vector<Eigen::Vector2i, Eigen::aligned_allocator<Eigen::Vector2i>>
      projected_points;

  ArrayXf x, y, z;
  x = Eigen::ArrayXf::Zero(points.size());
  y = Eigen::ArrayXf::Zero(points.size());
  z = Eigen::ArrayXf::Zero(points.size());

  for (size_t i = 0; i < points.size(); ++i) {
    x[i] = points[i][0];
    y[i] = points[i][1];
    z[i] = points[i][2];
  }

  float fov_up = (fov_up_ * M_PI) / 180.f;
  float fov_down = (fov_down_ * M_PI) / 180.f;
  float fov = std::abs(fov_down) + std::abs(fov_up);

  ArrayXf depth = x.square() + y.square() + z.square();
  depth = depth.sqrt();

  ArrayXf yaw = y.binaryExpr(
      x, [](const float& y, const float& x) { return std::atan2(y, x); });

  ArrayXf pitch = z / depth;
  pitch = pitch.asin();

  ArrayXf proj_x = (0.5f * (1.f - (yaw / M_PI))) * float(width_);
  ArrayXf proj_y = (1.f - ((pitch + fov_up) / fov)) * float(height_);

  ArrayXi u = ToIntAndClamp(0, width_ - 1, proj_x);
  ArrayXi v = ToIntAndClamp(0, height_ - 1, proj_y);

  for (size_t i = 0; i < points.size(); ++i) {
    projected_points.push_back(Eigen::Vector2i(u[i], v[i]));
  }

  return projected_points;
};
