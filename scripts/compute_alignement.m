%clear;
%clc;
data = csvread("ipbcar_100.txt");
name = '100';

gps = data(:, 1:3);
lidar = data(:, 4:6);

mu_gps = sum(gps, 1) / size(gps, 1);
mu_lidar = sum(lidar, 1) / size(lidar, 1);

sigma = zeros(3, 3);

for i = 1: size(gps, 1)
    sigma = sigma + (lidar(i, :) - mu_lidar)' * (gps(i, :) - mu_gps);
end

sigma = sigma / size(lidar, 1);

disp(sigma);

[U, S, V] = svd(sigma);

if(det(U) * det(V) < 0)
   W =  eye(3,3);
   W(3,3) = -1;
else
    W = eye(3,3);
end

R = U * W * V';
t = mu_lidar' - R * mu_gps';

disp(R);
disp(t);

gps_in_lidar_frame = zeros(size(gps));

for i = 1: size(gps, 1)
    gps_in_lidar_frame(i, :) = (R * gps(i, :)' + t)';
end

clf;
plot(lidar(:, 1), lidar(:, 2));
hold on;
plot(gps_in_lidar_frame(:, 1), gps_in_lidar_frame(:, 2), 'Color','r');
legend("lidar estimation", "GNSS estimation", 'Location', 'best');
xlabel("x [m]");
ylabel("y [m]");
title(name);
axis equal;
saveas(gcf,name,'epsc'); 
% pause();
% clf;
% plot3(lidar(:, 1), lidar(:, 2), lidar(:, 3));
% hold on;
% plot3(gps_in_lidar_frame(:, 1), gps_in_lidar_frame(:, 2), gps_in_lidar_frame(:, 3), 'Color','r');
% legend("lidar estimation", "gps estimation");

mse_error = 0;
mse_xy_error = 0;

for i = 1: size(lidar, 1)
    error = lidar(i,:) - gps_in_lidar_frame(i,:);
    xy_error = lidar(i,1:2) - gps_in_lidar_frame(i,1:2);
    mse_error = mse_error + error * error';
    mse_xy_error = mse_xy_error + xy_error * xy_error';
end

mse_error = sqrt(mse_error) / size(lidar, 1)
mse_xy_error = sqrt(mse_xy_error) / size(lidar, 1)
