seq_name_list = ["ipbcar_00.txt", "ipbcar_01.txt", "ipbcar_02.txt", "ipbcar_03.txt", "ipbcar_04.txt", "ipbcar_05.txt", "ipbcar_06.txt", "ipbcar_07.txt",...
    "ipbcar_08.txt","ipbcar_09.txt","ipbcar_10.txt","ipbcar_11.txt","ipbcar_12.txt","ipbcar_13.txt","ipbcar_14.txt","ipbcar_15.txt","ipbcar_16.txt","ipbcar_17.txt"];
plot_name = ["00", "01","02", "03","04", "05","06", "07","08", "09","10", "11","12", "13","14", "15","16", "17"];
mse_error_tot = {};
mse_xy_error_tot = {};

addpath("../dataset/")

for i = 1:size(seq_name_list,2)
    %data = seq_name_list(i);
    data = csvread(seq_name_list(i));
    name = plot_name(i);
    compute_alignement;
    mse_error_tot{end+1} = mse_error;
    mse_xy_error_tot{end+1} = mse_xy_error;
end